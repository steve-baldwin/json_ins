create table invoice(
    id uuid primary key default gen_random_uuid(),
    buyer_id uuid,
    amount int,
    purchase_order text,
    created date default now()
    );

create table invoice_detail(
    id uuid primary key default gen_random_uuid(),
    invoice_id uuid,
    unit_price int,
    quantity int,
    subtotal int,
    sku text
    );

create table buyer(
    id uuid,
    available_credit int
    );

insert
into    buyer(id, available_credit)
values  ('97c7e306-4726-4ad6-9ba5-20c924cc3731', 2147483647)
;
