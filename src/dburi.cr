require "uri"

class DBUri < URI
  def initialize(**args)
    self.scheme = "postgres"
    self.user = args["user"]? || ENV["PGUSER"]
    self.password = args["password"]? || ENV["PGPASSWORD"]?
    self.host = args["host"]? || ENV["PGHOST"]?
    self.port = ENV["PGPORT"]?.try &.to_i
    self.path = "/" + ENV["PGDATABASE"]
  end
end
