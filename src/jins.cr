require "./dburi"
require "db"
require "pg"
require "logger"
require "json"

module Jins
  PAYLOAD = <<-END
    {
      "buyer_id": "97c7e306-4726-4ad6-9ba5-20c924cc3731",
      "amount": 12345,
      "purchase_order": "abc123",
      "details": [
        {
          "unit_price": 12,
          "quantity": 2,
          "subtotal": 24,
          "sku": "a1"
        },
        {
          "unit_price": 23,
          "quantity": 1,
          "subtotal": 23,
          "sku": "b2"
        }
      ]
    }
  END
  SQL_1 = <<-END
  INSERT
  INTO    invoice(buyer_id, amount, purchase_order)
  VALUES  ( $1, $2, $3 )
    RETURNING id, amount
  END
  SQL_2 = <<-END
  INSERT
  INTO    invoice_detail(invoice_id, unit_price, quantity, subtotal, sku)
  VALUES  ( $1, $2, $3, $4, $5 )
  END
  SQL_3 = <<-END
  UPDATE  buyer
  SET     available_credit = available_credit - $1
  WHERE   id    = $2
  END
  SQL_4 = <<-END
  SELECT  ins_from_json($1)
  END

  class InvoiceDetail
    include JSON::Serializable
    property unit_price : Int32
    property quantity : Int32
    property subtotal : Int32
    property sku : String
  end

  class Invoice
    include JSON::Serializable
    property buyer_id : String
    property amount : Int32
    property purchase_order : String
    property details : Array(InvoiceDetail)
  end

  class Runner
    property log = Logger.new(STDOUT)
    property db : DB::Database

    def initialize
      t_start = Time.now
      progname = File.basename(PROGRAM_NAME)
      t_prev = t_start
      @log = Logger.new(STDOUT)
      @log.level = Logger::WARN
      @log.formatter = Logger::Formatter.new do |severity, datetime, _, message, io|
        total_time = datetime - t_start
        delta_time = datetime - t_prev
        t_prev = datetime
        io.printf(
          "%s|%s|%s|%09.6f|%09.6f|%s|%s",
          severity.to_s[0],
          datetime.to_s("%H:%M:%S.%L"),
          progname,
          total_time.total_seconds,
          delta_time.total_seconds,
          Fiber.current.name,
          message
        )
      end
      @log.warn("Connecting")
      @db = DB.open DBUri.new
      @log.warn("Connected")
    end

    def run
      payload = Invoice.from_json(PAYLOAD)
      @db.transaction do |trx|
        dbt = trx.connection
        t1 = Time.now
        1000.times do
          rslt = dbt.query_one?(SQL_1, payload.buyer_id, payload.amount, payload.purchase_order, as: {invoice_id: String, amount: Int32})
          if rslt
            payload.details.each do |d|
              dbt.exec(SQL_2, rslt[:invoice_id], d.unit_price, d.quantity, d.subtotal, d.sku)
            end
            dbt.exec(SQL_3, rslt[:amount], payload.buyer_id)
          end
        end
        t2 = Time.now
        @log.warn("Old : %9.6f" % (t2 - t1).total_seconds)
        trx.rollback
      end

      @db.transaction do |trx|
        dbt = trx.connection
        t1 = Time.now
        1000.times do
          invoice_id = dbt.query_one?(SQL_4, PAYLOAD, as: String)
        end
        t2 = Time.now
        @log.warn("New : %9.6f" % (t2 - t1).total_seconds)
        trx.rollback
      end
    end
  end

  Runner.new.run
end
