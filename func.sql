create or replace function ins_from_json(i_payload in jsonb)
    returns uuid
    set search_path to public
    language plpgsql
as $code$
declare
    l_inv_id    uuid;
    l_inv_amt   int;
    l_buyer_id  uuid;
begin
    insert
    into    invoice(buyer_id, amount, purchase_order)
    select  *
    from    jsonb_to_record(i_payload) as x(buyer_id uuid, amount int, purchase_order text)
        returning id, amount, buyer_id into l_inv_id, l_inv_amt, l_buyer_id
    ;
    --
    insert
    into    invoice_detail(invoice_id, unit_price, quantity, subtotal, sku)
    select  l_inv_id, x.*
    from    jsonb_to_recordset(i_payload->'details') as x(unit_price int, quantity int, subtotal int, sku text)
    ;
    --
    update  buyer
    set     available_credit    = available_credit - l_inv_amt
    where   id                  = l_buyer_id
    ;
    return l_inv_id;
end;
$code$